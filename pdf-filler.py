#!/usr/bin/env python3

import os
import pdfrw
import sys
import json
import argparse
from datetime import date

ANNOT_KEY = '/Annots'
ANNOT_FIELD_KEY = '/T'
ANNOT_VAL_KEY = '/V'
ANNOT_RECT_KEY = '/Rect'
SUBTYPE_KEY = '/Subtype'
WIDGET_SUBTYPE_KEY = '/Widget'

parser = argparse.ArgumentParser()
parser.add_argument("inpdf")
parser.add_argument("outpdf")
parser.add_argument("data", nargs='?')
parser.add_argument("--date", help="Automatically set 'Date' field to the current date (dd/mm/yyyy)", action="store_true")
args = parser.parse_args()
print("PDF Filler")
if args.date:
    print("Date on !")

# Open & Read PDF
template_pdf = pdfrw.PdfReader(args.inpdf)
annotations = template_pdf.pages[0][ANNOT_KEY]

# If Data is passed via argv
if args.data:
    data = json.loads(args.data)
    for annotation in annotations:
        if annotation[SUBTYPE_KEY] == WIDGET_SUBTYPE_KEY:
            if annotation[ANNOT_FIELD_KEY]:
                key = annotation[ANNOT_FIELD_KEY][1:-1]
                if key in data.keys():
                    annotation.update(
                    pdfrw.PdfDict(V='{}'.format(data[key]))
                    )
                elif args.date and key == "Date":
                    today = date.today().strftime("%d/%m/%Y")
                    annotation.update(
                    pdfrw.PdfDict(V='{}'.format(today))
                    )
                else:
                    print("Missing data: " + key)
                    exit(1)
# Else ask it via CLI
else:
    for annotation in annotations:
        if annotation[SUBTYPE_KEY] == WIDGET_SUBTYPE_KEY:
            if annotation[ANNOT_FIELD_KEY]:
                key = annotation[ANNOT_FIELD_KEY][1:-1]
                try:
                    if args.date:
                        value = date.today().strftime("%d/%m/%Y")
                    else:
                        value = input("Field required -> " + key + ": ")
                except:
                    print("Missing data: " + key)
                    exit(1)
                annotation.update(
                pdfrw.PdfDict(V='{}'.format(value))
                )
pdfrw.PdfWriter().write(args.outpdf, template_pdf)
